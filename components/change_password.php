<div class="form-section">
	<div class="form-grid">
		<form method="POST" action="">
			<div class="group">
				<h2 class="form-heading">Change Password</h2>
			</div>

			<div class="group">
				<input type="password" name="current_password" class="control" placeholder="Current password..." value="<?php if(isset($current_password)): echo $current_password; endif; ?>">
				<div class="name-error error"><?php echo (isset($current_password_error) ? $current_password_error : ''); ?></div>
			</div>

			<div class="group">
				<input type="password" name="new_password" class="control" placeholder="New password..." value="<?php if(isset($new_password)): echo $new_password; endif; ?>">
				<div class="name-error error"><?php echo (isset($new_password_error) ? $new_password_error : ''); ?></div>
			</div>

			<div class="group">
				<input type="password" name="confirm_password" class="control" placeholder="Confirm New password..." value="<?php if(isset($confirm_password)): echo $confirm_password; endif; ?>">
				<div class="name-error error"><?php echo (isset($confirm_password_error) ? $confirm_password_error : ''); ?></div>
			</div>

			<div class="group">
				<input type="submit" name="change_password" class="btn account-btn" value="Save Changes">
			</div>

		</form>
	</div>
</div>