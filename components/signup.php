<div class="form-area">
	<form method="POST" action="" enctype="multipart/form-data">
		<div class="group">
			<h2 class="form-heading">Create new account</h2>
		</div>

		<div class="group">
			<input type="text" name="full_name" class="control" placeholder="Enter Full name..." value="<?php if(isset($full_name)): echo $full_name; endif; ?>">
			<div class="name-error error"><?php echo (isset($name_error) ? $name_error : ''); ?></div>
		</div>

		<div class="group">
			<input type="email" name="email" class="control" placeholder="Enter Email..." autocomplete="off" value="<?php if(isset($email)): echo $email; endif; ?>">
			<div class="name-error error"><?php echo (isset($email_error) ? $email_error : ''); ?></div>
		</div>

		<div class="group">
			<input type="password" name="password" class="control" placeholder="Create password..." autocomplete="off" value="<?php if(isset($password)): echo $password; endif; ?>">
			<div class="name-error error"><?php echo (isset($password_error) ? $password_error : ''); ?></div>
		</div>

		<div class="group">
			<label for="file" id="file-label"><i class="fas fa-cloud-upload-alt upload-icon"></i>Choose image</label>
			<input type="file" name="img" class="file" id="file">
			<div class="name-error error"><?php echo (isset($image_error) ? $image_error : ''); ?></div>
		</div>

		<div class="group">
			<input type="submit" name="signup" class="btn account-btn" value="Create Account">
		</div>
		<div class="group">
			<a href="login.php" class="link">Already have an account?</a>
		</div>
	</form>
</div><!--close-form-area-->