<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/chat.js"></script>
<script>
	$(document).ready(function(){
		$('.custom-bars').click(function() {
			$('#sidebar').slideToggle();
		});

		$('.remove').click(function(){
			$('.flash').hide();
		})

		setTimeout(function(){
			$('.flash').fadeOut("slow");
		}, 5000);

		$(document).on("change", ".change-img", function(){
			var image_name = $(".change-img").val();
			var file = image_name.split("\\").pop();
			$('#change-image-label').html(file);
		});
	});
</script>