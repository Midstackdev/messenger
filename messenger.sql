-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2019 at 09:05 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `messenger`
--

-- --------------------------------------------------------

--
-- Table structure for table `clean`
--

CREATE TABLE `clean` (
  `id` int(11) NOT NULL,
  `clean_msg_id` int(11) NOT NULL,
  `clean_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clean`
--

INSERT INTO `clean` (`id`, `clean_msg_id`, `clean_user_id`) VALUES
(1, 32, 7),
(2, 29, 2),
(3, 18, 3);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `msg_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `msg_type` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `msg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`msg_id`, `message`, `msg_type`, `user_id`, `msg_time`) VALUES
(1, 'hi lori', 'text', 2, '2019-03-15 18:21:30'),
(2, 'i miss you', 'text', 2, '2019-03-15 18:23:50'),
(3, 'Alfred Web Developer Resume.pdf', 'pdf', 2, '2019-03-15 20:18:37'),
(4, 'assets/emoji/u1F61B.png', 'emoji', 2, '2019-03-15 20:50:32'),
(6, 'Whats good bro', 'text', 7, '2019-03-18 10:38:11'),
(7, 'we need to talk', 'text', 7, '2019-03-18 10:38:25'),
(8, 'come online bro', 'text', 7, '2019-03-18 10:38:41'),
(9, 'another one', 'text', 7, '2019-03-18 11:30:45'),
(10, 'bac.jpg', 'jpg', 7, '2019-03-18 12:14:16'),
(11, 'me again', 'text', 2, '2019-03-18 12:29:36'),
(12, 'modern-table-design-html-css-template.zip', 'zip', 7, '2019-03-18 12:40:39'),
(13, 'Booking.com_ Confirmation.pdf', 'pdf', 7, '2019-03-18 12:53:13'),
(14, 'assets/emoji/u1F60B.png', 'emoji', 7, '2019-03-18 13:38:18'),
(15, 'assets/emoji/u1F63B.png', 'emoji', 7, '2019-03-18 13:46:09'),
(16, 'Delaides Business Plan.docx', 'docx', 7, '2019-03-18 13:48:57'),
(17, 'JB Properties.xlsx', 'xlsx', 7, '2019-03-18 13:49:04'),
(18, 'hilo', 'text', 3, '2019-03-18 14:22:04'),
(19, 'assets/emoji/u1F60E.png', 'emoji', 2, '2019-03-18 14:28:07'),
(20, 'UW_staticIP.PNG', 'PNG', 2, '2019-03-18 14:49:55'),
(21, 'Supplements.jpg', 'jpg', 2, '2019-03-18 14:52:24'),
(22, 'CSS3Tables.zip', 'zip', 2, '2019-03-18 14:55:40'),
(23, 'you good', 'text', 7, '2019-03-18 16:30:37'),
(24, 'hi again', 'text', 7, '2019-03-18 16:49:58'),
(25, 'hiking is good', 'text', 7, '2019-03-18 16:50:23'),
(26, 'assets/emoji/u1F61B.png', 'emoji', 7, '2019-03-18 16:56:02'),
(27, 'assets/emoji/u1F60E.png', 'emoji', 2, '2019-03-18 16:56:36'),
(28, 'UML_use_case_example-800x707.PNG', 'PNG', 2, '2019-03-18 16:56:50'),
(29, 'hello', 'text', 2, '2019-03-19 11:26:22'),
(30, 'you good', 'text', 7, '2019-03-19 11:27:28'),
(31, 'how about now', 'text', 7, '2019-03-19 11:31:46'),
(32, 'hiloo', 'text', 7, '2019-03-19 11:50:25'),
(33, 'pealamb forms.pdf', 'pdf', 7, '2019-03-19 14:05:01'),
(34, 'Booking.com_ Confirmation.pdf', 'pdf', 7, '2019-03-19 14:07:41'),
(35, 'assets/emoji/u1F60E.png', 'emoji', 7, '2019-03-19 14:36:19'),
(36, 'assets/emoji/u1F63C.png', 'emoji', 7, '2019-03-19 14:49:00'),
(37, 'assets/emoji/u1F61C.png', 'emoji', 7, '2019-03-19 14:49:07'),
(38, 'am back', 'text', 2, '2019-03-19 15:01:47'),
(39, 'again I guess', 'text', 2, '2019-03-19 15:01:56'),
(40, 'assets/emoji/u1F62D.png', 'emoji', 2, '2019-03-19 15:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `clean_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `image`, `status`, `clean_status`) VALUES
(1, 'admin', 'admin@admin.com', 'password', '', 0, 0),
(2, 'bosso losso', 'bosso@gmail.com', '$2y$10$4qNvpTyqYbL8m5DU4D5JZuT3g2Sz.VCKNZ7W7jMwg50bx5OwK6Lry', '6.jpg', 0, 1),
(3, 'lfrdsmit', 'lfrdsmit@gmail.com', '$2y$10$CA.I6HALrZhdjytXMiXNfOYCs9W4pVwZLizp11/DVy7WhGNBz5Bwy', '1_L5QyrMNalM3yhtgdgBcvkQ.png', 0, 1),
(5, 'lfrdsmit', 'lfrdsmit@ocs.com', '$2y$10$uWr3rkBDgksj56j.WFesC.l.Q9uZ/ViLfzTCdoUTKV4EMW1ZyLzWq', '1_L5QyrMNalM3yhtgdgBcvkQ.png', 0, 0),
(6, 'admino', 'bosso@gm.com', '$2y$10$21iFbqxTUAh7omOWK2eX.OyY.DRmHqBzGbQ1E14nqIajvQkkZ6PXO', '1_L5QyrMNalM3yhtgdgBcvkQ.png', 0, 0),
(7, 'bakuka', 'baku@gmail.com', '$2y$10$N.UbtdN8X8Q7JOlhtmjKcekoDa85egeGs5Bp/RFD9Z0nfZ.EEZBk.', '5.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_activities`
--

CREATE TABLE `users_activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_activities`
--

INSERT INTO `users_activities` (`id`, `user_id`, `login_time`) VALUES
(1, 7, '1553006740'),
(2, 2, '1553955447');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clean`
--
ALTER TABLE `clean`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msg_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_activities`
--
ALTER TABLE `users_activities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clean`
--
ALTER TABLE `clean`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users_activities`
--
ALTER TABLE `users_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
