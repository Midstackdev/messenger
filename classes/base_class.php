<?php 

class base_class extends db {

	private $Query; 

	public function Normal_Query($query, $param = null){
		if(is_null($param)){
			$this->Query = $this->con->prepare($query);
			return $this->Query->execute();
		}else{
			$this->Query = $this->con->prepare($query);
			return $this->Query->execute($param);
		}
	}

	public function Count_Rows(){
		return $this->Query->rowCount();
	}

	public function fetch_all(){
		return $this->Query->fetchAll(PDO::FETCH_OBJ);
	}

	public function security($data){
		return trim(strip_tags($data));
	}

	public function Create_Session($session_name, $session_value){
		$_SESSION[$session_name] = $session_value;
	}

	public function fetch_one(){
		return $this->Query->fetch(PDO::FETCH_OBJ);
	}

	public function time_ago($db_msg_time){

		date_default_timezone_set("Africa/Accra");
		
		$db_time = strtotime($db_msg_time);

		$current_time = time();

		$seconds = $current_time - $db_time;
		$minutes = floor($seconds/60);
		$hours	 = floor($seconds/3600); // 60 * 60
		$days    = floor($seconds/ 86400); // 24 *60 * 60
		$weeks	 = floor($seconds / 604800); // 7 * 24 * 60 * 60
		$months	 = floor($seconds / 25920000); // 30 * 24 * 60 * 60
		$years	 = floor($seconds / 315360000); // 365 * 24 * 60 * 60

		if($seconds <= 60){
			return 'Jsut Now';
		}
		else if($minutes <= 60){

			if($minutes == 1){
				return "1 minute ago";
			}else{
				return $minutes. " minutes ago";
			}
		}
		else if($hours <= 24){

			if($hours == 1){
				return "1 hour ago";
			}else{
				return $hours. " hours ago";
			}

		}
		else if($days <= 7){

			if($days == 1){
				return "1 day ago";
			}else{
				return $days. "days ago";
			}
		}
		else if($weeks <= 4.3){

			if($weeks == 1){
				return "1 week ago";
			}else{
				return $weeks. " weeks ago";
			}

		}
		else if($months <= 12){

			if($months == 1){
				return "1 month ago";
			}else{
				return $months. " months ago";
			}
		}
		else{
			if($years == 1){
				return "1 year ago";
			}else{
				return $years. " years ago";
			}
		}
	}

	/*public function validation_message($session_name, $session_value){

		echo '<div class="flash success-flash">
				<span class="remove">&times;</span>
				<div class="flash-heading">
					<h3><span class="checked">&#10004;</span>Sucess: you have done</h3>
				</div>
				<div class="flash-body">
					<p>'.$this->Create_Session($session_name, $session_value).'</p>
				</div>
			</div><!--/success flash-->';
	}

	public function set_message($message){

		if(!empty($message)){

			$_SESSION['message'] = $message;
		}else {

			$message = "";
		}
	}
	
	public function display_message(){

		if(isset($_SESSION['message'])){

			echo $_SESSION['message'];

			unset($_SESSION['message']);
		}
	}
	*/	


}



?>